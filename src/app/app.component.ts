import {Component, OnInit} from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Title } from "@angular/platform-browser";
import * as jQuery from 'jquery';
import { TranslateService } from "@ngx-translate/core";

import {StorageService} from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'SOS Support';
  themeClass: string= 'escort-theme';

  constructor(private translate: TranslateService,
              private overlayContainer: OverlayContainer,
              private _title: Title)
  {
    translate.setDefaultLang('en');
  }

  ngOnInit()
  {
    let locale = 'en'
    this.translate.addLangs(['en']);
    this.translate.use(locale);
    let newthemeClass = this.getBrandName();

    this.themeClass = newthemeClass;

    this.overlayContainer.getContainerElement().classList.add(newthemeClass);
  }

  getBrandName(){
    let hostName = window.location.hostname;

    let hostParts = hostName.split('.');

    let subDomain = hostParts[0];

    let theme;
    switch (subDomain) {
      case 'localhost':
      default :
        this._title.setTitle('SOS Support');

        jQuery('#favicon').attr("href",'/fleet.ico');

        theme = 'escort-theme';
    }

    StorageService.setTheme(theme);

    return theme;
  }
}

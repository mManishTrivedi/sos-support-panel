import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClient } from '@angular/common/http';

//System modules
import "hammerjs";
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

//Base Modules
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Base services
import { MiErrorHandler } from './services/error.service';

//Pop-ups
import { AlertBoxModule } from './pop-ups/alert-box/alert-box.module';
import { AuthenticationService } from './auth/authentication.service';
import { LoggedinGuard } from './auth/guards/loggedin.guard';
import { SelectivePreloadingStrategy } from './selective-preloading-strategy';
import { HttpInterceptorProviders } from './services/http-interceptors';
import { LoggingInterceptor } from './services/http-interceptors/logger.interceptor';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    //System Module
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,

    //System Modules
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),

    //Base Module
    AppRoutingModule,

    //Pop-ups
    AlertBoxModule
  ],
  providers: [
    AuthenticationService,
    LoggedinGuard,
    SelectivePreloadingStrategy,
    HttpInterceptorProviders,
    { provide: HTTP_INTERCEPTORS, useClass: LoggingInterceptor, multi: true },
    { provide: ErrorHandler, useClass: MiErrorHandler },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

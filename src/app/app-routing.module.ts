import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { SelectivePreloadingStrategy } from "./selective-preloading-strategy";

import { LoggedinGuard }from './auth/guards/loggedin.guard';

const routes : Routes = [

  { path: 'login',
    loadChildren: './auth/login/login.module#LoginModule'
  },

  { path: 'logout',
    loadChildren: './auth/login/login.module#LoginModule'
  },

  { path: '',
    loadChildren: './mi-modules/main/main.module#MainModule',
    canActivate: [ LoggedinGuard ],
    data: {
      preload: true,
    }
  },

  //In case of any unkown route
  { path: '404', loadChildren: './common/pagenotfound/pagenotfound.module#PagenotfoundModule' },
  { path: '**', loadChildren: './common/pagenotfound/pagenotfound.module#PagenotfoundModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,
        {
          enableTracing: false, // <-- debugging purposes only
          preloadingStrategy: SelectivePreloadingStrategy,
        })
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

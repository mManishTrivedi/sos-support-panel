/**
 * @author Mohit Agrawal<Mohit.Agrawal@mi-xlab.com>
 * @copyright Motherson Invenzen X-Lab Pvt.Ltd. 2017
 */

//TODO :: not tested at
import { Pipe, PipeTransform } from '@angular/core';
/*import {MiConstants} from "../constants/constants";*/

@Pipe({
  name: 'miunitconversion'
})
export class MiUnitConversionPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any
  {
    let conversion;


    if(Array.isArray(args)) {
      conversion = args[0];
      args.splice(0,1);
    }

    if(!conversion || conversion == void(0)){
      return value;
    }
    if(conversion == 'mtokm') {
      return this.convertMeterToKm(value, args);
    }

    if(conversion == 'millistominhr') {
      return this.convertMillisToMinHr(value);
    }

    if(conversion == 'timeago') {
      return this.convertTimestampToTimeago(value);
    }

    if(conversion == "mpsToKmph") {
      return this.convertMPStoKMPH(value);
    }

    if(conversion == "idelingtominhr"){
      return this.convertIdealingToMinHr(value);
    }

    if(conversion == "separateWithComma"){
      return this.formatNumberToLocal(value, args);
    }

    if(conversion == 'millistohrmin') {
      return this.convertMillisToMin(value);
	  }

    if(conversion == 'secondstomin') {
      return this.convertSecondsToMinutes(value, args);
    }

    if(conversion == 'phnumberstonumber') {
      return this.convertPhoneNumberToNumber(value);
    }

    if(conversion == 'remainingtime') {
      return this.convertTimestampToRemainingTime(value);
    }

    return value;
  }

  convertMeterToKm(value, pipeLineData)
  {
    let with_unit;
    with_unit = (pipeLineData.length == 0 || pipeLineData[0] == void(0) || pipeLineData[0] == true) ? true : false;
    value = parseInt(value);

    if(value < 1000) {
      return (with_unit) ? (value) + ' m' : value;
    }

    return (with_unit)? (value/1000).toFixed(2) + ' km' : (value/1000).toFixed(2);
  }

  convertMillisToMinHr(value)
  {
    let value_sec = parseInt(value);

    value_sec = value_sec/1000;

    //1 min
    if(value_sec < 60) {
      return (value_sec%60).toFixed(0) + ' sec';
    }

    //1 hr
    if(value_sec < 3600)
    {
      let value_min = (value_sec/60).toFixed(0);
      return value_min.toString()+ ' min';
    }

    if(value_sec < 86400)
    {
      let value_hr = Math.floor(value_sec/3600);
      let value_min = ((value_sec%3600)/60).toFixed(0);

      return value_hr.toString()+ ' hr '+value_min.toString()+ ' min';
    }

    let value_days = Math.floor(value_sec/86400);
    let value_hr = Math.floor((value_sec % 86400)/3600);
    let value_min = (((value_sec % 86400) %3600)/60).toFixed(0);

    return value_days.toString() +' day ' + value_hr.toString() + ' hr ' + value_min.toString() + ' min';
  }

  convertTimestampToTimeago(value)
  {
	if(value == void(0)) {
		  return '';
    }

    let timestamp = parseInt(value);

    let current_time = Date.now();

    let remaining_time = current_time-timestamp;

    if(remaining_time <= 0) {
      return this.convertMillisToMinHr(0);
    }

    return this.convertMillisToMinHr(remaining_time);
  }

  convertMPStoKMPH(value)
  {
    let number;

    if(value == void(0) || value == 0 || Number.isNaN((parseInt(value)))) {
      return 'N/A';
    }

    number = (value * 36)/10;
    return Math.ceil(number) + " Km/h";
  }

  convertIdealingToMinHr(value)
  {
    if(!value || value == 0){
      return 0;
    }

    let total_ideling = parseInt(value);

    let total_min = total_ideling*2;

    if(total_min < 60) {
      return (total_min%60).toFixed(0) + ' min';
    }

    if(total_min < 1440)
    {
      let value_hr = Math.floor(total_min/60);
      let value_min = (total_min%60).toFixed(0);

      return value_hr.toString()+ ' hr '+value_min.toString()+ ' min';
    }

    let value_hr = ((total_min)/60).toFixed(0);
    let value_min = ((total_min % 1440) %60).toFixed(0);

    return value_hr.toString() + ' hr ' + value_min.toString() + ' min';
  }

  formatNumberToLocal(value, pipeLineData)
  {
    let number, language;

    if(!value || value == void(0) || Number.isNaN((parseFloat(value)))) {
      return value;
    }

    language = (pipeLineData.lengh == 0 || !pipeLineData[0] || pipeLineData[0] == void(0))
        ? 'en'
        : pipeLineData[0];

    number = (value).toLocaleString(language);
    return number;
  }

  convertMillisToMin(value)
  {
    let value_sec = parseInt(value);

    value_sec = value_sec/1000;

    //1 min
    if(value_sec < 60) {
      return Math.floor(value_sec%60) + ' sec';
    }

    //1 hr
    if(value_sec < 3600)
    {
      let value_min = Math.floor(value_sec/60);
      return value_min.toString()+ ' min';
    }

    if(value_sec < 86400)
    {
      let value_hr = Math.floor(value_sec/3600);
      let value_min = Math.floor((value_sec%3600)/60);

      return value_hr.toString()+ ' hr '+value_min.toString()+ ' min';
    }
    let value_hr = Math.floor((value_sec)/3600);
    let value_min = Math.floor(((value_sec % 86400) %3600)/60);

    return value_hr.toString() + ' hr ' + value_min.toString() + ' min';
  }

  convertSecondsToMinutes(value, pipeLineData)
  {
    let value_sec = parseInt(value);

    let with_unit;
    with_unit = (pipeLineData.length == 0 || pipeLineData[0] == void(0) || pipeLineData[0] == true) ? true : false;

    if(isNaN(value_sec) || value_sec == 0 ){
      return (with_unit) ? 0 + ' min' : 0;
    }

    return (with_unit) ? Math.ceil(value_sec/60) + ' min' : Math.ceil(value_sec/60);
  }

  convertPhoneNumberToNumber(value)
  {
    let number = value;
    return number.replace(/[^\d]/g, '');
  }

  convertTimestampToRemainingTime(value)
  {
    if(value == void(0)) {
      return '';
    }

    let timestamp = parseInt(value);

    let current_time = Date.now();

    let remaining_time = timestamp-current_time;

    if(remaining_time <= 0) {
      return this.convertMillisToMinHr(0);
    }

    return this.convertMillisToMinHr(remaining_time);
  }
}

/**
 * Custom search filter pipe
 *
 * Uses:
 * {{Date / Timestamp String | midateformat : 'DATE_FORMAT_SUPPORTED_BY_MOMENT' }}
 *
 *
 * @author Prafful Panwar<Prafful.Panwar@mi-xlab.com>
 * @copyright Motherson Invenzen X-Lab Pvt.Ltd. 2017
 */
import { Pipe, PipeTransform } from '@angular/core';
import * as MiMoment from 'moment';
import {StorageService} from "../services/storage.service";

@Pipe({
  name: 'midateformat'
})
export class MiDateFormatPipe implements PipeTransform {

  transform(value: any, formatType: string = ""): any
  {
    let format, locale;

    locale = StorageService.getLanguage('en');

    if (!value) {
        console.log('Invalid date Conversion', {value : value, format : formatType});
        return "";
    }

    value = parseInt(value);

    switch (formatType) {
      case 'short' :  //example  (9:03 AM, 15/6/15)
        switch (locale) {
          case 'ja' :
            format = 'YY/MM/DD, hh:mm A';
            break;
          case 'en' :
          default :
            format = 'hh:mm A, DD/MM/YY';
            break;
        }
        break;
      case 'semiMedium' :  //example  (9:03 AM, 15 Jun, 15).
        switch (locale) {
          case 'ja' :
            format = 'YY, MMM DD, hh:mm A';
            break;
          case 'en' :
          default :
            format = 'hh:mm A, DD MMM, YY';
            break;
        }
        break;
      case 'medium' : //example  (9:03 AM, 15 Jun, 2015).
        switch (locale) {
          case 'ja' :
            format = 'YYYY MMM DD, hh:mm A';
            break;
          case 'en' :
          default :
            format = 'hh:mm A, DD MMM YYYY';
            break;
        }
        break;
      case 'long' : //example (9:03:01 AM, 15 Jun, 2015).
        switch (locale) {
          case 'ja' :
            format = 'YYYY MMM DD, hh:mm:ss A';
            break;
          case 'en' :
          default :
            format = 'hh:mm:ss A, DD MMM YYYY';
            break;
        }
        break;
      case 'full' :  //example  (Monday, 15 June, 2015 at 9:03:01 AM GMT+01:00).
        switch (locale) {
          case 'ja' :
            format = 'YYYY MMMM DD, hh:mm:ss A ZZ';
            break;
          case 'en' :
          default :
            format = 'dddd, DD MMMM YYYY, hh:mm:ss A ZZ';
            break;
        }
        break;
      case 'shortWithoutYear' :  //example  (9:03 AM, 15/6)
        switch (locale) {
          case 'ja' :
            format = 'MM/DD, hh:mm A';
            break;
          case 'en' :
          default :
            format = 'hh:mm A, DD/MM';
            break;
        }
        break;
      case 'mediumWithoutYear' : //example  (9:03 AM, 15 Jun).
        switch (locale) {
          case 'ja' :
            format = 'MMM DD, hh:mm A';
            break;
          case 'en' :
          default :
            format = 'hh:mm A, DD MMM';
            break;
        }
        break;
      case 'longWithoutYear' : //example (9:03:01 AM, 15 Jun).
        switch (locale) {
          case 'ja' :
            format = 'MMM DD, hh:mm:ss A';
            break;
          case 'en' :
          default :
            format = 'hh:mm:ss A, DD MMM';
            break;
        }
        break;
      case 'fullWithoutYear' :  //example  (Monday, 15 June, 9:03:01 AM GMT+01:00).
        switch (locale) {
          case 'ja' :
            format = 'MMMM DD, hh:mm:ss A ZZ';
            break;
          case 'en' :
          default :
            format = 'dddd, DD MMMM, hh:mm:ss A ZZ';
            break;
        }
        break;
      case 'shortDate' : //example  (15/6/15).
        switch (locale) {
          case 'ja' :
            format = 'YY/MM/DD';
            break;
          case 'en' :
          default :
            format = 'DD/MM/YY';
            break;
        }
        break;
      case 'semiMediumDate' : //example  (15 Jun, 15).
        switch (locale) {
          case 'ja' :
            format = 'YY MMM DD';
            break;
          case 'en' :
          default :
            format = 'DD MMM YY';
            break;
        }
        break;
      case 'mediumDate' : //example  (15 Jun, 2015).
        switch (locale) {
          case 'ja' :
            format = 'YYYY MMM DD';
            break;
          case 'en' :
          default :
            format = 'DD MMM YYYY';
            break;
        }
        break;
      case 'longDate' : //example (15 June, 2015).
        switch (locale) {
          case 'ja' :
            format = 'YYYY MMMM DD';
            break;
          case 'en' :
          default :
            format = 'DD MMMM YYYY';
            break;
        }
        break;
      case 'fullDate' :  //example  (Monday, 15 June, 2015).
        switch (locale) {
          case 'ja' :
            format = 'YYYY MMMM DD, dddd';
            break;
          case 'en' :
          default :
            format = 'dddd, DD MMMM YYYY';
            break;
        }
        break;
        case 'shortDateWithoutYear' : //example  (15/6).
            switch (locale) {
                case 'ja' :
                    format = 'MM/DD';
                    break;
                case 'en' :
                default :
                    format = 'DD/MM';
                    break;
            }
            break;
        case 'semiMediumDateWithoutYear' : //example  (15 Jun).
            switch (locale) {
                case 'ja' :
                    format = 'MMM DD';
                    break;
                case 'en' :
                default :
                    format = 'DD MMM';
                    break;
            }
            break;
        case 'mediumDateWithoutYear' : //example  (15 Jun).
            switch (locale) {
                case 'ja' :
                    format = 'MMM DD';
                    break;
                case 'en' :
                default :
                    format = 'DD MMM';
                    break;
            }
            break;
        case 'longDateWithoutYear' : //example (15 June).
            switch (locale) {
                case 'ja' :
                    format = 'MMMM DD';
                    break;
                case 'en' :
                default :
                    format = 'DD MMMM';
                    break;
            }
            break;
        case 'fullDateWithoutYear' :  //example  (Monday, 15 June).
            switch (locale) {
                case 'ja' :
                    format = 'MMMM DD, dddd';
                    break;
                case 'en' :
                default :
                    format = 'dddd, DD MMMM';
                    break;
            }
            break;
      case 'shortTime' : //example  (9:03 AM).
        switch (locale) {
          case 'ja' :
            format = 'hh:mm A';
            break;
          case 'en' :
          default :
            format = 'hh:mm A';
            break;
        }
        break;
      case 'mediumTime' : //example  (9:03:01 AM).
        switch (locale) {
          case 'ja' :
            format = 'hh:mm:ss A';
            break;
          case 'en' :
          default :
            format = 'hh:mm:ss A';
            break;
        }
        break;
      case 'longTime' : //example (9:03:01 AM GMT+1).
        switch (locale) {
          case 'ja' :
            format = 'hh:mm:ss A z';
            break;
          case 'en' :
          default :
            format = 'hh:mm:ss A z';
            break;
        }
        break;
      case 'fullTime' : //example (9:03:01 AM GMT+01:00)
        switch (locale) {
          case 'ja' :
            format = 'hh:mm:ss A zz';
            break;
          case 'en' :
          default :
            format = 'hh:mm:ss A zz';
            break;
        }
        break;
    }

    return MiMoment(value).format(format);
  }
}

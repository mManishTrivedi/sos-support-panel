import { NgModule } from '@angular/core';

import { AlertBoxComponent } from "./alert-box.component";
import { MiCommonModule } from "../../common/mi-common/mi-common.module";

@NgModule({
	imports: [
		MiCommonModule,
	],
	declarations: [AlertBoxComponent],
	entryComponents: [
		AlertBoxComponent
	]
})
export class AlertBoxModule { }

import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CommonModule } from "@angular/common";

export interface DialogData {
	heading: string;
	message: string;
	button: string;
}

@Component({
	selector: 'alert-box-dialog',
	templateUrl: './alert-box-dialog.html',
})
export class AlertBoxComponent {

	constructor(
		public dialogRef: MatDialogRef<AlertBoxComponent>,
		@Inject(MAT_DIALOG_DATA) public data: DialogData
	) {}
 }

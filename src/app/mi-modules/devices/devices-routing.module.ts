import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuard} from '../../auth/guards/role.guard';
import { DevicesComponent } from './devices.component';
import { ListComponent } from './list/list.component';
import { UpdateComponent } from './view/update.component';

const routes

: Routes = [{
	path: '',
	component: DevicesComponent,
		children: [
	    {path: '', redirectTo: 'list', pathMatch: 'full'},
      {path: 'list', component: ListComponent},
      {path: ':serial_number', component: UpdateComponent},
    ]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DevicesRoutingModule {}

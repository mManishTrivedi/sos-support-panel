import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {BackButtonModule} from '../../common/back-button/back-button.module';

import { DevicesComponent } from './devices.component';
import { ListComponent } from './list/list.component';
import { DevicesRoutingModule } from './devices-routing.module';
import { HeaderModule } from '../../common/header/header.module';
import { MiCommonModule } from '../../common/mi-common/mi-common.module';
import { PaginationModule } from '../../common/pagination/pagination.module';
import { UpdateComponent } from './view/update.component';
import { DebugDataBoxComponent } from './debug-data/debug-data-box.component';

@NgModule({
  imports: [
    RouterModule,
    DevicesRoutingModule,
    HeaderModule,
    MiCommonModule,
    PaginationModule,
    BackButtonModule
  ],
	declarations: [ DevicesComponent, ListComponent, UpdateComponent, DebugDataBoxComponent ],
  entryComponents: [DebugDataBoxComponent]
})
export class DevicesModule { }

import { Overlay } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, Sort } from '@angular/material';
import { DebugDataBoxComponent } from '../debug-data/debug-data-box.component';
import { DevicesService } from '../devices.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as MiLodash from 'lodash';
import * as MiMoment from "moment-timezone";
// @ts-ignore
import { default as _rollupMoment, Moment } from 'moment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatDatepicker } from '@angular/material/datepicker';

const moment = _rollupMoment || MiMoment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MMM, YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-device-view',
  templateUrl: 'update.component.html',
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class UpdateComponent implements OnInit {

    date = new FormControl(moment());
    public eventFilter: FormGroup;
    constructor(private _deviceService: DevicesService,
                private _router: Router,
                private _activeRoute: ActivatedRoute,
                private _dialog: MatDialog,
                private _overlay: Overlay,
                private _fb: FormBuilder
    ) {
        this.eventFilter = _fb.group({
            event_name: new FormControl('', Validators.compose([]))
        })
    }
    public order_by = 'e_received';
    public order = 'desc';
    private events = [];
    public filter_events = [];
    public serial_number;
    private event_name;
    public noData = false;
    debugData;

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.getVehicleEvents();
  }

	ngOnInit() {
    this.serial_number = this._activeRoute.snapshot.params['serial_number'];
    this.getVehicleEvents();
  }

  sortData(sort: Sort) {
    this._sortData(sort.active, sort.direction)
  }

  _sortData(sort_by, sort) {
      let order_by = [];
      let order = [];
      order_by.push(sort_by);
      order.push(sort);
      let sortedData = MiLodash.orderBy(this.events, order_by, order);
      this.filter_events = sortedData;
  }

  onSearchPress(event)
  {
    let filterData = MiLodash.filter(this.events, function (item) {
    return item.e_name.indexOf(event) > -1;
    });
    this.filter_events = filterData;
  }


  async getVehicleEvents() {
      this.noData = false;
      let data = {
          serial_number: this.serial_number,
          date: this.date.value.valueOf(),
      };
      let response = await this._deviceService.getVehiclesEvents(data);
      this.events = response['data'].sosEvents;
      if (this.events.length === 0) {
          this.noData = true;
      }
      this._sortData(this.order_by, this.order);
  }


  async showDebugData(event, index) {
      let nxt_event = this.events[index + 1];
      let data = {
          end_time: Date.now(),
          start_time: event.e_occur,
          serial_number: this.serial_number
      };

      if (nxt_event) {
          data.end_time = nxt_event.e_occur
      }

      let response = await this._deviceService.getDebugData(data);
      const scrollStrategy = this._overlay.scrollStrategies.reposition();
      let dialogRef = this._dialog.open(DebugDataBoxComponent, {
          panelClass: 'mi-debug-data-box',
          data: {
              debugData: response['data'].records
          },
          role: 'dialog',
          scrollStrategy: scrollStrategy,
          minWidth: "80vw"
      });
  }
}

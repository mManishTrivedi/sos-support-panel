import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CommonModule } from "@angular/common";

export interface DialogData {
	debugData
}

@Component({
	selector: 'debud-data-box-dialog',
	templateUrl: './debug-data-box-dialog.html',
})
export class DebugDataBoxComponent {

	constructor(
		public dialogRef: MatDialogRef<DebugDataBoxComponent>,
		@Inject(MAT_DIALOG_DATA) public data: DialogData
	) {}
 }

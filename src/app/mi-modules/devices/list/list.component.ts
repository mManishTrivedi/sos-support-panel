import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Sort} from '@angular/material';
import { DevicesService } from '../devices.service';
import {ActivatedRoute, Router} from '@angular/router';

let intervalTimer: any;

@Component({
  selector: 'app-device-list',
  templateUrl: 'list.component.html',
})
export class ListComponent implements OnInit {

  public vehicleListFilter:FormGroup;
	constructor(private _deviceService: DevicesService,
              private _router: Router,
              private _active_route: ActivatedRoute,
              private _fb: FormBuilder) {
	  this.vehicleListFilter = _fb.group({
      serial_number: new FormControl('', Validators.compose([])),
      mobile: new FormControl('', Validators.compose([])),
      imei: new FormControl('', Validators.compose([])),
      imsi: new FormControl('', Validators.compose([])),
      firmware: new FormControl('', Validators.compose([])),
      oem: new FormControl('', Validators.compose([])),
    });

    let serial_number = this._active_route.snapshot.queryParamMap.get("serial_number");
    if(serial_number) {
      this.filter.serial_number = serial_number;
      this.vehicleListFilter.patchValue({
        serial_number
      })
    }
    let imei = this._active_route.snapshot.queryParamMap.get("imei");
    if(imei) {
      this.filter.imei = imei;
      this.vehicleListFilter.patchValue({
        imei
      })
    }
    let imsi = this._active_route.snapshot.queryParamMap.get("imsi");
    if(imsi) {
      this.filter.imsi = imsi;
      this.vehicleListFilter.patchValue({
        imsi
      })
    }
    let mobile = this._active_route.snapshot.queryParamMap.get("mobile");
    if(mobile) {
      this.filter.mobile = mobile;
      this.vehicleListFilter.patchValue({
        mobile
      })
    }
    let firmware = this._active_route.snapshot.queryParamMap.get("firmware");
    if(firmware) {
      this.filter.firmware = firmware;
      this.vehicleListFilter.patchValue({
        firmware
      })
    }
    let oem = this._active_route.snapshot.queryParamMap.get("oem");
    if(oem) {
      this.selectedOem = oem;
      this.vehicleListFilter.patchValue({
        oem
      })
    }
    let page_number = this._active_route.snapshot.queryParamMap.get("page_number");
    if(page_number) {
      this.pageNumber = parseInt(page_number)
      this.pageIndex = this.pageNumber -1;

    }
    let limit = this._active_route.snapshot.queryParamMap.get("limit");
    if(limit) {
      this.itemPerPage = parseInt(limit);
    }
    let order_by = this._active_route.snapshot.queryParamMap.get("order_by");
    if(order_by) {
      this.order_by = order_by;
    }
    let order = this._active_route.snapshot.queryParamMap.get("order");
    if(order) {
      this.order = order;
    }

    this.updateQueryParams();;
  }
  public vehicles = [];
  public totalRecords = 0;
  public itemPerPage = 10;
  public pageNumber = 1;
  public pageIndex = this.pageNumber -1;
  public order_by = 'last_connected';
  public order = 'desc';
  private filter = {
    serial_number: null,
    mobile: null,
    imei: null,
    imsi: null,
    firmware: null
  };
  private selectedOem;
  public oems = [
    {name : 'mi-xlab'},
    {name: 'escorts'}
  ];
  public noData = false;
	ngOnInit() {
	  this.getVehicles();
  }

  async getVehicles()
  {
    this.noData = false;
    let data = {
      page_number: this.pageNumber,
      limit: this.itemPerPage,
      order_by: this.order_by,
      order: this.order
    };

    if(this.filter.firmware) {
      data['firmware'] = this.filter.firmware
    }
    if(this.filter.imei) {
      data['imei'] = this.filter.imei;
    }
    if(this.filter.imsi) {
      data['imsi'] = this.filter.imsi;
    }
    if(this.filter.mobile) {
      data['mobile'] = this.filter.mobile
    }
    if(this.filter.serial_number) {
      data['serial_number'] = (this.filter.serial_number).trim()
    }
    if(this.selectedOem) {
      data['oem'] = this.selectedOem;
    }
    let response = await this._deviceService.getVehicles(data);
    if(response.code != 200) {
      throw response;
    }

    this.vehicles = response['data'].vehicles;
    this.totalRecords = response['data'].total_records;
    if(this.vehicles.length === 0) {
      this.noData = true;
    }
  }

	redirectToDetails(serial_number) {
    this._router.navigate(['/devices/'+ serial_number]);
  }

  onPageChange(event)
  {
    this.itemPerPage = event.pageSize;
    this.pageNumber = event.pageIndex;
    this.getVehicles();
    this.updateQueryParams();
  }

  sortData(sort: Sort)
  {
    this.order_by = sort.active;
    this.order = sort.direction;
    this.updateQueryParams();
    this.getVehicles();
  }

  updateQueryParams()
  {
    let queryParams = {
      page_number: this.pageNumber,
      limit: this.itemPerPage,
      order_by: this.order_by,
      order: this.order,
      serial_number: this.filter.serial_number,
      imei: this.filter.imei,
      imsi: this.filter.imsi,
      firmware: this.filter.firmware,
      phone: this.filter.mobile,
      oem: this.selectedOem
    };
    this._router.navigate(
      [],
      {
        relativeTo: this._active_route,
        queryParams: queryParams
      });
  }

  onSearchPress(event, event_name)
  {
    this.filter[event_name] = (event) ? event : null;
    clearInterval(intervalTimer);
    intervalTimer = setTimeout(()=>
    {
      this.updateQueryParams();
      this.getVehicles();
    }, 1000)
  }

  onOemChange(oem)
  {
    this.selectedOem = (oem.value === 'all') ? null : oem.value;

    this.getVehicles();
    this.updateQueryParams();
  }
}

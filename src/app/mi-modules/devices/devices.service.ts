import { Injectable } from '@angular/core';
import { RestclientService } from '../../services/restclient.service';

@Injectable({
  providedIn: 'root'
})
export class DevicesService {

  constructor(
    private _client_service: RestclientService
  ) {}

  getVehicles(data) {
    return this._client_service.get('api/escorts/vehicles/list', data);
  }

  getVehiclesEvents(data) {
    return this._client_service.get('api/escorts/vehicles/details', data);
  }

  getDebugData(data) {
    return this._client_service.get('api/admin/debug-data', data);
  }
}

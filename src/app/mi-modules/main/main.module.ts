import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { DevicesModule } from '../devices/devices.module';
import { HeaderModule } from '../../common/header/header.module';

@NgModule({
	imports: [
		RouterModule,
    MainRoutingModule,
    DevicesModule,
    HeaderModule,
    TranslateModule.forChild(),
	],
	declarations: [ MainComponent ]
})
export class MainModule { }

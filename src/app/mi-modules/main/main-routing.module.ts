import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: 'devices', pathMatch: 'full'},
  { path: 'devices',
    loadChildren: './../devices/devices.module#DevicesModule',
    data: {
      preload: true,
    }
  }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MainRoutingModule {}

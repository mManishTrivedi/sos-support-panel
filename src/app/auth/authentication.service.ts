import { Injectable } from '@angular/core';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService 
{
  private is_logged_in = false;
  private user_roles = [];

  constructor() { }

  public getToken()
  {
    return StorageService.getToken();
  }

  public isLoggedIn()
  {
    try {
        let access_token = this.getToken();
        if (access_token) {
            return true;
        }

        return false;
    }catch (e) {
        this.doLogout();
        return false;
    }
  }

  public doLogin(login_details) {
    let {token, locale, name } = login_details;

    StorageService.setToken(token);
    StorageService.setLanguage(locale);
    StorageService.setUserName(name);

    return true;
  }

  public doLogout()
  {
    let theme = StorageService.getTheme();
    StorageService.clearLocalStorage();
    StorageService.setTheme(theme);
    return true;
  }
}

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../authentication.service';
import * as MiLodash from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate
{

  constructor(private _auth_service: AuthenticationService, private _router: Router) {};

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean
    {
      //TODO :: check url access
      return true;
    }
}

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedinGuard implements CanActivate {

  constructor(private _auth_service: AuthenticationService, private _router: Router) {};

  canActivate(): boolean {
    if(this._auth_service.isLoggedIn()) {
      return true;
    }

    this._router.navigate(['/login']);
    return false;
  }
}

import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormControl}  from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from "./login.service";
import { StorageService } from "../../services/storage.service";
import { AuthenticationService } from '../authentication.service';
import { MatDialog } from "@angular/material";
import { AlertBoxComponent } from '../../pop-ups/alert-box/alert-box.component';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit
{

	private landing_page = '/devices/list';
	public loginFormGroup;
	public localeList = [
		{key: 'en', value: "English (en-us)"}
	];
	//This variable is used because when logout html is loaded and but loginFormGroup is not available so it end up with error.
	//there could be alternative to do this in ngOnInit but not tried.
	public show_login = true;
	public logo_url:string = '../../assets/img/rollr_login.png';
	public ppUrl = 'https://rollr.io/privacy-policy/';
	public tcUrl= 'https://rollr.io/terms-conditions/';

	constructor(private _formBuilder : FormBuilder,
	          private _auth_service: AuthenticationService,
	          private _router: Router,
	          private _login_service: LoginService,
		  private dialog: MatDialog)
	{
		if(this._auth_service.isLoggedIn())
		{
			if(this._router.url == '/logout')
			{
				this.show_login = false;
				this.doLogout();
			}
			else
            {
              this._router.navigate([this.landing_page]);
			}
		}
		else
		{
			this.loginFormGroup = this._formBuilder.group({
				username_ctrl: new FormControl('', Validators.compose([Validators.required, Validators.email, Validators.nullValidator])),
				password_ctrl: new FormControl('', Validators.compose([Validators.required, Validators.nullValidator])),
				language_ctrl: new FormControl('en', Validators.compose([Validators.required, Validators.nullValidator])),
				terms_ctrl: new FormControl('', Validators.compose([Validators.required, Validators.nullValidator])),
			});

			this.setThemeData();
		}
	}

	private email_error_msg = 'Email is incorrect';
	private password_error_msg = 'Password is incorrect';

	ngOnInit() {}

	getEmailErrorMessage() {
		return this.loginFormGroup.controls.username_ctrl.hasError('required') ? 'Email id is required' :
			this.loginFormGroup.controls.username_ctrl.hasError('email') ? 'Not a valid email' :
				this.loginFormGroup.controls.username_ctrl.hasError('nullValidator') ? this.email_error_msg :
					'';
	}

	getPasswordErrorMessage() {
		return this.loginFormGroup.controls.password_ctrl.hasError('required') ? 'Password is required.' :
			this.loginFormGroup.controls.password_ctrl.hasError('nullValidator') ? this.password_error_msg :
				'';
	}

	async onSubmit()
	{
		try
		{
			let data = {
				username: (this.loginFormGroup.controls.username_ctrl.value).toLowerCase(),
				password: this.loginFormGroup.controls.password_ctrl.value,
				locale: this.loginFormGroup.controls.language_ctrl.value
			};
			await this._login_service.login(data);
			this._router.navigate([this.landing_page]);
		} catch (e)
		{
			let errors = e.error.data;

			if(errors.length != 0){
				for(let error of errors) {
					let field_name = error.field;

					switch (field_name) {
						case 'email':
							this.loginFormGroup.controls.username_ctrl.setErrors({'nullValidator': true})
							this.email_error_msg = error.message;
							break;
						case 'password':
							this.loginFormGroup.controls.password_ctrl.setErrors({'nullValidator': true})
							this.password_error_msg = error.message;
							break;
						default:
							throw e;
					}
				}
			}else
			{
        let error = (e.error.message) ? e.error.message : "Invalid email or password";
				this.dialog.open(AlertBoxComponent, {
					panelClass : 'mi-login-denied',
					data: {
						message: (e.error.message) ? e.error.message : "Invalid email or password",
						button: 'ok'
					}
				});
			}
		}
	}

	async doLogout()
	{
		await this._login_service.logout();

		this._router.navigate(['login']);
	}

  setThemeData() {
    let theme = StorageService.getTheme();

    switch (theme) {
      case 'escort-theme':
      default :
      this.logo_url = '../../assets/img/rollr_login.png';
      this.ppUrl = 'https://rollr.io/privacy-policy/';
      this.tcUrl= 'https://rollr.io/terms-conditions/';
    }
  }
}

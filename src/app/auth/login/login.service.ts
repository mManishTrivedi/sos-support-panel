/**
 * Created by prafful on 31th July 2018.
 */
import { Injectable } from '@angular/core';
import { RestclientService } from "../../services/restclient.service";
import { StorageService } from "../../services/storage.service";
import { AuthenticationService } from "../authentication.service";

@Injectable()
export class LoginService
{
	constructor (private _rest_client: RestclientService,
              private _auth_service: AuthenticationService) {}

  public async login(data) {
    try {
      let response = await this._rest_client.post('api/users/login', data);

      let login_details= {
        token: response['data'].token,
        name: response['data'].profile.name,
        locale: data.locale
      };

      this._auth_service.doLogin(login_details);

      return true;
    }
    catch (err)
    {
      throw err;
    }
	}

  public async logout()
  {
    try {
      await this._rest_client.post('api/users/logout');

      await this._auth_service.doLogout();

      return true;
    }
    catch (err)
    {
      await this._auth_service.doLogout();
      return false;
    }
  }
}

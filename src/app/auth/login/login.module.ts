import { NgModule } from '@angular/core';

import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { LoginService } from "./login.service";
import { MiCommonModule } from '../../common/mi-common/mi-common.module';

@NgModule({
  imports: [
    LoginRoutingModule,
    MiCommonModule
  ],
  declarations: [LoginComponent],
  providers: [
	LoginService
  ]
})
export class LoginModule { }

import { Injectable } from '@angular/core';
import * as MiMoment from 'moment-timezone';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor() { }
  static userName = 'userName';
  static token = 'token';
  static locale = 'locale';
  static theme = 'theme';
  // Set token to local storage
  static setToken(tok){
  window.localStorage.setItem(this.token, tok);
  }
  // get token to local storage
  static getToken() {
    let token = window.localStorage.getItem(this.token);
    if(token) {
      return token;
    }
    return null;
  }
  // get user name from local storage
  static getUserName(default_value) {
		let userName = window.localStorage.getItem(this.userName);
		return (userName) ? userName : default_value;
	}

	// set user name to local storage
	static setUserName(value){
		window.localStorage.setItem(this.userName,value);
	}
  //clear local storage..
	static clearLocalStorage()
  {
		window.localStorage.clear();
	}

	static getLanguage(default_value) {
		let locale = window.localStorage.getItem(this.locale);

		return (locale) ? locale : default_value
	}

	static setLanguage(value) {
		window.localStorage.setItem(this.locale, value);
	}

	static setTheme(value) {
		window.localStorage.setItem(this.theme, value);
	}

	static getTheme() {
		return window.localStorage.getItem(this.theme);
	}

  static setData(key, data)
  {
    if(typeof data != 'string') {
      data = JSON.stringify(data);
    }
    window.localStorage.setItem(key, data);
  }

  static getData(key)
  {
    let data = window.localStorage.getItem(key);
    try {
      return JSON.parse(data);
    } catch (e) {
      return data;
    }
  }

  static removeData(key) {
    window.localStorage.removeItem(key);
  }
}

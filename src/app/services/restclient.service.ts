import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class RestclientService
{
  private _base_url: string = environment.rest_api_base_url;

  constructor(private _http: HttpClient, private _auth_service: AuthenticationService) {}

  public async get(api_path: string = '/', request_params: any = {}, responseType=null, serverUrl=null)
  {
    let {url, httpOptions} = this._processGetHttpOptions(api_path, request_params, responseType, serverUrl);

      httpOptions['observe']= 'response';
    return await this._http.get(url, httpOptions)
		.toPromise()
		.then((response)=>
		{
      return this.handleErrorCode(response)
		})
		.catch(this.handleError);
  }

  public post(api_path: string = '/', request_params: any = {})
  {
    let {url, httpOptions} = this._processPostHttpOptions(api_path);
    return this._http.post(url,request_params, httpOptions).toPromise()
      .then((response)=>
      {
        return this.handleErrorCode(response)
      }).catch(this.handleError);
  }

  public put(api_path: string = '/', request_params: any = {})
  {
    let {url, httpOptions} = this._processPostHttpOptions(api_path);
    return this._http.put(url, request_params, httpOptions).toPromise()
      .then((response)=>
      {
        return this.handleErrorCode(response)
      }).catch(this.handleError);
  }

  public delete(api_path: string = '/', request_params: any = {})
  {
    let {url, httpOptions} = this._processGetHttpOptions(api_path, request_params);
    return this._http.delete(url, httpOptions).toPromise()
      .then((response)=>
      {
        return this.handleErrorCode(response)
      }).catch(this.handleError);
  }

  private _processGetHttpOptions(api_path, request_params, responseType=null, serverUrl=null)
  {
	let request_data = this._convertAnyToHttp(request_params);

	let headerData = {};
	let access_token = this._auth_service.getToken();

	headerData['Content-Type'] =  'application/json'

	let url = this._base_url+'/'+api_path;
	if(serverUrl) {
	  url = serverUrl+'/'+api_path;
	}


	if(!serverUrl && access_token) {
	  headerData['']
	  headerData['Authorization'] = access_token;
	}

	let httpOptions = {
	  headers: new HttpHeaders(headerData),
	  params : request_data
	};

	if(responseType) {
	  httpOptions['responseType'] = responseType;
	}

	return {
	  url: url,
	  httpOptions: httpOptions
	};
  }

	private _processPostHttpOptions(api_path)
	{
		let headerData = {};
		let access_token = this._auth_service.getToken();

		headerData['Content-Type'] =  'application/json'

		if(access_token) {
			headerData['Authorization'] = access_token;
		}

		let httpOptions = {
			headers: new HttpHeaders(headerData)
		};

		let url = this._base_url+'/'+api_path;

		return {
			url: url,
			httpOptions: httpOptions
		};
	}

	public handleError (error)
	{
		throw error;
	}

	_convertAnyToHttp(params: {}): { [param: string]: string | string[]; } {
		params = Object.assign({}, params);
		Object.keys(params).forEach(key => {
			if (typeof params[key] === 'object') {
				params[key] = JSON.stringify(params[key]);
			}
		});
		return params;
	}


    private _downloadFile(res){
        let contentType, file_name, blob, url, a_tag;

        contentType = res.headers.get('content-type');

        file_name = this._getFileName(res);

        blob = new Blob([res.body], { type: contentType });

        // for IE browser
        if (navigator.appVersion.toString().indexOf('.NET') > 0) {
            window.navigator.msSaveBlob(blob, file_name);
        }
        else {
            url = window.URL.createObjectURL(blob);

            a_tag = document.createElement("a");
            a_tag.href = url;
            a_tag.download = file_name;

            document.body.appendChild(a_tag);
            a_tag.click();

            setTimeout(()=>{
                window.URL.revokeObjectURL(url);
            },1000);
        }
    }

    private _getFileName(res)
    {
        let contentDispositionHeader, file_name;

        contentDispositionHeader = res.headers.get('Content-Disposition');

        file_name = contentDispositionHeader.split(';')[1].trim().split('=')[1];

        return file_name.replace(/"/g, '');
    }

    private handleErrorCode(data)
    {
      // Login response handle
      if(data.data) {
          return data;
      }
      if(data.body && data.body.code) {
        if(data.body.code === 200) {
          return data.body;
        }

        let error = data.body;
        let error_obj = {
          status: error['code'],
          message: error['message'],
          error: {
            data: [],
            message: error['message']
          }
        };

        if(error.code == 422) {
          if(error.error.info) {
            error_obj.error.message = error.error.info.message;
          }
        }
        throw error_obj;
      } else {
          if(data.code) {
            if(data.code === 200) {
              return data;
            }
            throw {
                status: data['code'],
                message: data['message'],
                error: {
                    data: [],
                    message: data['message']
                }
            };
          }else {
              throw {
                  status: 500,
                  message: 'Something is going wrong',
                  error: {
                      data: [],
                      message: 'Something is going wrong'
                  }
              };
          }
      }
    }
}

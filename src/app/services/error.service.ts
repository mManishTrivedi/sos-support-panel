import {Injectable, Injector, NgZone} from '@angular/core';
import { ErrorHandler } from "@angular/core";
import { AlertBoxComponent } from "../pop-ups/alert-box/alert-box.component";
import { MatDialog } from "@angular/material";
import { AuthenticationService } from "../auth/authentication.service";
import {Router} from "@angular/router";

@Injectable()
export class MiErrorHandler implements ErrorHandler
{
	private isBoxOpen = false;
	constructor(public dialog: MatDialog,
	            private ngZone: NgZone,
		private _auth_service: AuthenticationService,
		private _injector: Injector){}

	handleError(error)
	{
		if(error.rejection) {
            error = error.rejection;
        } else
        {
            console.error(error);
            return false;
        }

        console.error(error);
        let status = error.status;
        let message = (error.message) ? error.message: "Something is going wrong";
        let heading = "Error";
        let button = "Ok";
        let errorData = (error.error && error.error.data) ? error.error.data : [];

        if(Array.isArray(errorData)) {
            errorData = errorData[0];
        }

    switch (status) {
      case 401:
        heading= "Unauthorized access to resource";
        message = error.error.message;
        break;
      case 422:
        message = error.error.message;
        break;
      case 403:
        this._auth_service.doLogout();
        message = "Session expired. Please re-login";
        break;
      case 404:
        message = "Oops! page not found";
        break;
      case 500:
        message = error.error.message;
        break;
      default :
        break;
    }

        let data = {
          //heading: heading,  //intentionally disable heading because in case of invalid validation error internal field are shown
          message: message,
          button: button
        }

        //Hack Ref:- https://github.com/angular/material2/issues/7550
        this.ngZone.run(() => {
          this.alert(data, status)
        });
	}

	alert(data, status)
	{
		if(this.isBoxOpen) {
			return false;
		}

		if(status == 403) {
			let _router = this._injector.get(Router)
			_router.navigate(['login']);
			return true;
		}

		this.isBoxOpen = true;
		let dialogRef = this.dialog.open(AlertBoxComponent, {
			panelClass : 'mi-alert-dialog',
			data: data
		});

		dialogRef.afterClosed().subscribe(result => {
			this.isBoxOpen = false;
		});
	}
}

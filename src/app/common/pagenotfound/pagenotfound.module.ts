import { NgModule } from '@angular/core';

import { PagenotfoundComponent } from './pagenotfound.component';
import {RouterModule, Routes} from "@angular/router";

const routes : Routes = [
    {
        path: '',
        component: PagenotfoundComponent
    }
];

@NgModule({
  imports: [
      RouterModule.forChild(routes)
  ],
  declarations: [PagenotfoundComponent],
  exports: [
      RouterModule
  ]
})
export class PagenotfoundModule { }

import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pagenotfound',
  templateUrl: 'pagenotfound.component.html'
})
export class PagenotfoundComponent implements OnInit {

  constructor(private _location: Location) { }

  ngOnInit() {
  }

    goBack() {
        this._location.back();
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { MaterialComponentsModule } from '../material-components/material-components.module';
import { MiUnitConversionPipe } from "../../pipes/unit-conversion.pipe";
import { MiDateFormatPipe } from "../../pipes/midate-formatter.pipe";
import { DateRangePickerModule } from '../date-range-picker/date-range-picker.module';

@NgModule({
	imports: [
		CommonModule,
		MaterialComponentsModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
    		TranslateModule,
		DateRangePickerModule
	],
  declarations: [MiUnitConversionPipe, MiDateFormatPipe],
	exports: [
		MaterialComponentsModule,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		MiUnitConversionPipe,
		MiDateFormatPipe,
    		TranslateModule,
		DateRangePickerModule
	],
	providers: [Validators, MiDateFormatPipe, MiUnitConversionPipe]
})
export class MiCommonModule { }

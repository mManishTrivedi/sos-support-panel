import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';

@Component({
	selector: 'mi-pagination',
	templateUrl: 'pagination.component.html',
})
export class PaginationComponent implements OnInit{
	@Input() public totalItems;
	@Input() public pageSize;
	@Input() public pageIndex;
	@Input() public _pageSizeOptions;
	@Output() public page = new EventEmitter();

	public pageSizeOptions = [5, 10, 25, 100];

	ngOnInit(): void {
		this.pageSizeOptions = (this._pageSizeOptions) ? this._pageSizeOptions : this.pageSizeOptions;
	}

	public onPageChange(event) {
		event.pageIndex = event.pageIndex+1;
		this.page.emit(event);
	}
}

import { NgModule } from '@angular/core';

import { MaterialComponentsModule } from "./../material-components/material-components.module";
import { PaginationComponent } from "./pagination.component";

@NgModule({
	imports: [
		MaterialComponentsModule
	],
	declarations: [PaginationComponent],
	exports: [PaginationComponent, MaterialComponentsModule]
})
export class PaginationModule { }
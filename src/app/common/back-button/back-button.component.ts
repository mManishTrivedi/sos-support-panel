import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
	selector: 'app-back-button',
	templateUrl: './back-button.component.html'
})

export class BackButtonComponent {

	@Input()
	class: string = '';
	@Input()
	is_disable = false;

	constructor(private _location: Location){}

	goBack()
	{
		this.is_disable = true;
		this._location.back();
	}

	stopTrigger()
	{
		return false;
	}
}

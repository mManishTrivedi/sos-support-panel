import { NgModule } from '@angular/core';

import { BackButtonComponent } from "./back-button.component";
import { MiCommonModule } from "../mi-common/mi-common.module";

@NgModule({
	imports: [
		MiCommonModule
	],
	declarations: [BackButtonComponent],
	exports: [BackButtonComponent]
})
export class BackButtonModule { }
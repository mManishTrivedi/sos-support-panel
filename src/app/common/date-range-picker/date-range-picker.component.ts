/*
* Date and time range picker
* Reference from :: https://github.com/dangrossman/bootstrap-daterangepicker
*/

import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import * as moment from 'moment';

declare let jQuery: any;

@Component({
    selector: 'app-date-range',
    templateUrl: './date-range-picker.component.html'
})
export class DateRangeComponent implements OnInit {

    constructor() {}

    public name = 'date-range-name';

    @Input()
    public id = 'date-range-id';

    @Input()
    public start_time = Date.now();

    @Input()
    public class='';

    @Input()
    public end_time = Date.now();

    @Input()
    public single_date_pick = false;

    @Input()
    public withTime = false;

    @Input()
    public timePickerIncrementRange = 10;

    @Input()
    public min_date = 1469989800000;

    @Input()
    public max_date = null;

    @Input()
    public datePickerOpens = "right";

    @Input()
    public datePickerDrops = "down";

    @Input()
    public isTodayFilterRequired = true;

    @Input()
    public isYesterdayFilterRequired = true;

    @Input()
    public daysLimit = null;

  @Input()
  public isUtc = false;

    @Output()
    public dateRangeChanged:EventEmitter<any> = new EventEmitter<any>();

    public selected_date = 'Today';
    private date_format = 'DD MMM, YY';

    private previous_start = 0;
    private previous_end = 0;


    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges)
    {
      if(this.isUtc)
      {
        if(this.previous_start && this.previous_start == changes.start_time.currentValue) {
          return true;
        }
      }

        //It update for the first time also, so to avoid that duplicacy we inserted reset thing, but it's not working properly.
        // if(changes['reset'] && changes['reset']['previousValue'] == false && changes['reset']['currentValue'] == true) {}
        this.setDate(this.start_time, this.end_time);
    }

    ngAfterViewInit()
    {
		if(this.isUtc)
		{
			let offset = (moment().utcOffset())* 60000;
			this.start_time = this.start_time - offset
			this.end_time = this.end_time - offset
		}

        this.setDate(this.start_time, this.end_time);
    }

    setDate(start_ts, end_ts)
    {
        let start_time, end_time;

        start_time = start_ts;
        end_time = end_ts;
        if(!start_ts){
            start_time = Date.now().toString();
        }
        if(!end_ts){
            end_time = Date.now().toString();
        }
        end_time = parseInt(end_time);
        start_time = parseInt(start_time);
        this.min_date =  moment(this.min_date).valueOf();

        if(this.single_date_pick){
            end_time = start_time;
        }
        if(this.withTime) {
            this.date_format = 'hh:mm A, DD MMM YY';
        }
        if(start_time < this.min_date){
            start_time = this.min_date;
        }
        if(end_time < this.min_date){
            end_time = this.min_date;
        }
        if(this.max_date){
            if(end_time > this.max_date){
                end_time  = this.max_date;
            }
            if(start_time > this.max_date){
                start_time  = this.max_date;
            }
        }

        if((moment(start_time).startOf('day').valueOf() == moment().startOf('day').valueOf())
            && (moment(start_time).startOf('day').valueOf() == moment(end_time).startOf('day').valueOf())) {
            this.selected_date = (this.withTime) ? moment(start_time).format(this.date_format) :'Today'
        }
        else{
            this.selected_date = (this.single_date_pick)
                ? moment(start_time).format(this.date_format)
                : moment(start_time).startOf('day').format(this.date_format)+' - '+moment(end_time).startOf('day').format(this.date_format);
        }

        if(!start_ts && !end_ts){
            this.selected_date = (this.single_date_pick) ? "Select date" : "Select date range";
        }

        let custom_range_time = parseInt(Date.now().toString());

        let custom_range = {};

        if(this.isTodayFilterRequired)
        {
            custom_range["Today"]= [
                moment(custom_range_time).startOf('day').format(this.date_format),
                moment(custom_range_time).endOf('day').format(this.date_format)
            ];
        }

        if(this.isYesterdayFilterRequired)
        {
            custom_range["Yesterday"]= [
                moment(custom_range_time).subtract(1, 'day').startOf('day').format(this.date_format),
                moment(custom_range_time).subtract(1, 'day').endOf('day').format(this.date_format)
            ];
        }

        custom_range["Last 7 Days"] = [
            moment(custom_range_time).subtract(7, 'day').startOf('day').format(this.date_format),
            moment(custom_range_time).subtract(1, 'day').endOf('day').format(this.date_format)
        ];
        custom_range["Last 30 Days"]= [
            moment(custom_range_time).subtract(30, 'day').startOf('day').format(this.date_format),
            moment(custom_range_time).subtract(1, 'day').endOf('day').format(this.date_format)
        ];
        custom_range["This Month"]= [
            moment(custom_range_time).startOf('month').format(this.date_format),
            moment(custom_range_time).endOf('day').format(this.date_format)
        ];
        custom_range["Last Month"]= [
            moment(custom_range_time).subtract(1, 'month').startOf('month').format(this.date_format),
            moment(custom_range_time).subtract(1, 'month').endOf('month').format(this.date_format)
        ];


        let date_config = {};

        date_config["dateLimit"] = {
            days: (this.daysLimit) ? this.daysLimit : 59
        };

        date_config["ranges"]= custom_range;
        date_config["locale"]= {
            format: this.date_format
        };
        date_config["startDate"]= (!start_ts && !end_ts)
            ? moment(custom_range_time).startOf('week').format(this.date_format)
            : (this.withTime)
                ? moment(start_time).format(this.date_format)
                : moment(start_time).startOf('day').format(this.date_format);
        date_config["endDate"]= (!start_ts && !end_ts)
            ? moment(custom_range_time).endOf('day').format(this.date_format)
            : moment(end_time).endOf('day').format(this.date_format);
        date_config["minDate"]= (this.withTime)
            ? moment(this.min_date).format(this.date_format)
            : moment(this.min_date).startOf('day').format(this.date_format);
        date_config["maxDate"]= (this.max_date)
            ? (this.withTime)
                ? moment(this.max_date).format(this.date_format)
                : moment(this.max_date).endOf('day').format(this.date_format)
            : moment(custom_range_time).endOf('day').format(this.date_format);
        date_config["opens"]= this.datePickerOpens;
        date_config["drops"]= this.datePickerDrops;
        date_config["applyClass"]= "mat-flat-button";
        date_config["cancelClass"]= "mat-stroked-button";
        date_config["buttonClass"]= "";
        date_config["singleDatePicker"]= this.single_date_pick;
        date_config["timePicker"]= this.withTime;
        date_config["timePickerIncrement"]= this.timePickerIncrementRange;
        date_config["autoApply"]= true;

      jQuery('#'+this.id).daterangepicker(date_config,
            (start, end, label) =>
            {
                this.selected_date = (this.single_date_pick)
                    ? start.format(this.date_format)
                    : start.format(this.date_format)+' - '+end.format(this.date_format);

                //This is becuase we need time also that's why we send the start, end as it is.
                if(this.withTime)
                {
					let emitting_start = moment(start.valueOf());
					let emitting_end = moment(end.valueOf());

					if(this.isUtc) {
					  emitting_start.utc(true);
					  emitting_end.utc(true);
					}

					this.previous_start = emitting_start.valueOf();
					this.previous_end = emitting_start.valueOf();

                    this.dateRangeChanged.emit({start_time : emitting_start.valueOf(), end_time : emitting_end.valueOf()});
                }
                else
                {
					let emitting_start = moment(start.valueOf());
					let emitting_end = moment(end.valueOf());

					emitting_start.startOf('day');
					emitting_end.endOf('day');

					if(this.isUtc) {
					  emitting_start.utc(true);
					  emitting_end.utc(true);
					}

					this.previous_start = emitting_start.valueOf();
					this.previous_end = emitting_start.valueOf();

                    this.dateRangeChanged.emit({start_time : emitting_start.valueOf(), end_time : emitting_end.valueOf()});
                }
            });
    }

}

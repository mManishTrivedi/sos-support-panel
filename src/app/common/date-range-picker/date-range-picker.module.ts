import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material';

import { DateRangeComponent } from "./date-range-picker.component";

@NgModule({
  declarations: [DateRangeComponent],
  imports: [
    MatButtonModule
  ],
  exports: [DateRangeComponent]
})
export class DateRangePickerModule { }

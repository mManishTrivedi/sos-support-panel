import { NgModule } from '@angular/core';

import { HeaderComponent } from './header.component';
import { MiCommonModule } from "../mi-common/mi-common.module";

@NgModule({
  imports: [
    MiCommonModule
  ],
  declarations: [HeaderComponent],
  exports: [HeaderComponent]
})
export class HeaderModule { }

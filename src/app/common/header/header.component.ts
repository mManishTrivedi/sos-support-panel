import { Component, OnInit } from '@angular/core';
import { StorageService } from "../../services/storage.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() {
  	this.setLogoUrl()
  }

  public header_logo:string;
  user_name = 'Fleet';

  public mi_menus = [];
  public all_mi_menus =
	[
        {
            name: 'support',
            icon: 'account_circle',
            url: '/support',
            locale_text: 'support',
            sub_menus: [
                {
                    name: 'devices',
                    icon: '',
                    url: '/devices/list',
                    locale_text: 'devices'
                }
            ]
        }
    ];

  ngOnInit()
  {
    this.mi_menus = this.all_mi_menus;

  	this.user_name = StorageService.getUserName(this.user_name);
  }

	setLogoUrl()
  {
    let theme = StorageService.getTheme();

    switch (theme) {
      case 'escort-theme':
      default :
        this.header_logo = '../../assets/img/rollr_header.png';
    }

		return  true;
	}

}

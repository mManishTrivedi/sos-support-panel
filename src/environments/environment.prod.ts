export const environment = {
  production: true,
  rest_api_base_url: 'https://qa.rollr.io:4018'
};

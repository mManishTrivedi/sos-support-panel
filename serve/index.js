/**
 * Created by prafful on 01/09/17.
 */

var app, express, router, bodyParser, MiLodash, MiPromise, Mihttp, compression;

compression = require('compression')
express    = require('express');
app        = express();
bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

(process.env.PORT) ? app.set('port', process.env.PORT) : app.set('port', 8888);

app.use(compression());

// Middleware to Log every external hit to server
app.get('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    // res.setHeader("Cache-Control", "public, max-age=43200");
    // res.setHeader("Expires", new Date(Date.now() + 43200000).toUTCString());
    // res.header("Access-Control-Allow-Methods", "GET");

    console.log(req.method + " " + req.url);
    console.log("Host : " + req.headers.host + ".  User Agent : " + req.headers['user-agent']);
    next();
});

// Routes to server Static contents
app.use('/', express.static(require('path').join(__dirname, './public/')));

app.get('*', function (req, res) {
    var contents = require('fs').readFileSync(require('path').join(__dirname, './public/index.html')).toString();
    res.setHeader('Content-Type', 'text/html');
    res.send(new Buffer(contents));
});

app.listen(app.get('port'), function() {
    console.log('Web rollr server running on PORT : ' + app.get('port'));
});

# sos-support-panel

======================Format===============================

## vMajor.Minor.Patch (DD,MMM,YYYY)

**Current-CommitID:** _`COMMIT_ID`_

### Feature:
- Define new feature
### Change: 
- Define existing API Changes
### Fix:
- Bug Fixes
### Cron Setup/Update:
- Any kind of cron job
### Internal-Change:
- Internal flow changes
- Internal scripts
### Migration:
- Define migration
===========================================================
## v0.0.2 (6, June 2019)

**Current-CommitID:** _`cd892f768f31ff8f42320d27f46a595ae6cacab8`_
###Update:
- Filters on device event page
- Remove debug data button from device events page
- Remove profile section

###Fixes:
- UI fixes on device event page

## v0.0.1 (31, May 2019)

**Current-CommitID:** _`d88b9cb1e317f0f086658097020cd86338d0277a`_
###Feature:
- First production release with first phase components and pages
